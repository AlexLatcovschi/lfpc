/*example 20 (32 + 1 - 13)

Vn = { S, A, B, C }
Vt = { a, b, c, d }

P = {
1.	S->dA
2.	A->d
3.	A->aB
4.	B->bC
5.	C->cA
6.	C->aS
}*/

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

bool A(int i);
bool B(int i);
bool C(int i);
std::string word;
int index = 0;

bool init() {
	//cout << 'I';
	char n = word[index];
	if (n == 'd') return A(index + 1);
	return 0;
}

bool I(int i) {
	//cout << 'Z';
	index = i;
	char n = word[i];
	if (n == 'd') return A(index + 1);
	return 0;
}

bool A(int i) {
	//cout << 'A';
	index = i;
	char n = word[i];
	if (n == 'a') return B(++i);
	if ((n == 'd') & (word.size() > i + 1)) return C(++i);
	if (n == 'd') return 1;
	return 0;
}

bool B(int i) {
	//cout << 'B';
	index = i;
	char n = word[i];
	if ((n == 'b') & (word.size() > i + 1)) return C(++i);
	if (n == 'b') return 1;
	return 0;
}

bool C(int i) {
	//cout << 'C';
	index = i;
	char n = word[i];
	if (n == 'c') return A(++i);
	if (n == 'a') return I(++i);
	return 0;
}

int main() {
	std::cin >> word;
	cout << init();

	return 0;
}