#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;
// Varianta 20
vector<string> leftSide = {"S", "S", "S", "A", "A", "A", "A", "B", "B", "B", "B", "D", "C"};
vector<string> rightSide = {"aB", "bA", "A", "B", "Sa", "bBA", "b", "b", "bS", "aD", "epsilon", "AA", "Ba"};
int productionLength = 13;

void printArray(vector<string> arg, int length) {
    for (int n = 0; n < length; ++n)
        cout << arg[n] << " ";
    cout << "\n";
}

int searchEpsilon(vector<string> arr) {
    for (int i = 0; i < productionLength; i++) {
        if (arr[i] == "epsilon") {
            return i;
        }
    }
}

vector<int> searchInaccessibleArray(vector<string> arr, string value) {
    vector<int> returnArray;
    for (int i = 0; i < arr.size(); i++) {
        // check if item in array has nonterminal symbol
        if (arr[i] == value) {
            returnArray.push_back(i);
        }
    }
    return returnArray;
}

vector<string> shiftRight(vector<string> arr, int index, bool decrease) {
    for (int i = index; i < productionLength - 1; ++i) {
        arr[i] = arr[i + 1];
    }
    if (decrease) {
        productionLength--;
    }
    return arr;
}

vector<string> addItem(vector<string> arr, string item, bool increase, string type) {
//    cout << productionLength << " " << arr.size() << "\n";
    if (type == "inaccessible") {
        arr.push_back(item);
    } else {
        if (arr.size() == productionLength) {
            arr.push_back(item);
        } else {
            arr[productionLength] = item;
            if (increase) {
                productionLength++;
            }
        }
    }

    return arr;
}

void eliminationOfRenaming(vector<string> arr, string value) {
    // convert nonterminal string to char
    char nonterminal = value[0];
    for (int i = 0; i < rightSide.size(); i++) {
//        cout << rightSide[i] << "\n";
        for (int j = 0; j < rightSide[i].size(); j++) {
            if (rightSide[i][j] == nonterminal) {
                string element;
                for (int k = 0; k < rightSide[i].length(); k++) {
                    if (rightSide[i][k] != nonterminal) {
                        element += rightSide[i][k];
                    }
                }
                rightSide = addItem(rightSide, element, false, "");
                leftSide = addItem(leftSide, leftSide[i], true, "");
                if (rightSide[i].size() == 1) {
                    // remove productions
                    leftSide[i] = "";
                    rightSide[i] = "";
                    leftSide = shiftRight(leftSide, i, false);
                    rightSide = shiftRight(rightSide, i, true);
                    cout << "Remove zero productions: \n";
                    printArray(rightSide, productionLength);
                }
            }
        }
    }
}

void eliminateInaccessibleSymbols(vector<string> arr) {
    vector<int> inaccessibleIndexesTop;
    for (int i = 0; i < rightSide.size(); i++) {
        for (int j = 0; j < rightSide[i].size(); j++) {
            // check inaccessible symbols
            if (rightSide[i].size() == 1 && isupper(rightSide[i][0])) {
                vector<int> inaccessibleIndexes = searchInaccessibleArray(rightSide, rightSide[i]);
                inaccessibleIndexesTop = inaccessibleIndexes;
                for (int l = 0; l < inaccessibleIndexes.size(); l++) {
                    // search indexes in first array to swap with second one
                    vector<int> swapElements = searchInaccessibleArray(leftSide, rightSide[inaccessibleIndexes[l]]);
                    for (int index = 0; index < swapElements.size(); index++) {
                        // add new productions
                        rightSide = addItem(rightSide, rightSide[swapElements[index]], false, "inaccessible");
                        leftSide = addItem(leftSide, leftSide[inaccessibleIndexes[l]], true, "inaccessible");
                    }
                }
            }
        }
    }
    for (int i = 0; i < inaccessibleIndexesTop.size(); i++) {
        // remove productions
        leftSide[inaccessibleIndexesTop[i]] = "";
        rightSide[inaccessibleIndexesTop[i]] = "";
        leftSide = shiftRight(leftSide, inaccessibleIndexesTop[i], false);
        rightSide = shiftRight(rightSide, inaccessibleIndexesTop[i], true);
    }
}

void eliminateNonProductiveSymbols(vector<string> arr) {
    vector<string> addNewItems;
    for (int i = 0; i < rightSide.size(); i++) {
        for (int j = 0; j < rightSide[i].size(); j++) {
            // check terminal symbols symbols
            if (rightSide[i].size() == 1 && islower(rightSide[i][0])) {
                addNewItems.push_back(rightSide[i]);
            }
        }
    }
    // filter unique terminal symbols
    addNewItems.erase(unique(addNewItems.begin(), addNewItems.end()), addNewItems.end());

    // add new productions for terminal symbols
    vector<int> addedItemIndex;
    vector<string> addedItems;
    vector<string> symbols = {"X", "Y"};
    for (int i = 0; i < addNewItems.size() - 1; i++) {
        leftSide = addItem(leftSide, symbols[i], false, "inaccessible");
        rightSide = addItem(rightSide, addNewItems[i], true, "inaccessible");
        addedItemIndex.push_back(rightSide.size() - 1);
        addedItems.push_back(addNewItems[i]);
    }

    //swap terminal symbols in productions
    for (int i = 0; i < rightSide.size(); i++) {
        for (int j = 0; j < addedItemIndex.size(); j++) {
            for (int k = 0; k < rightSide[i].size(); k++) {
                if (rightSide[i][k] == addedItems[j][0] && rightSide[i].size() > 1) {
                    rightSide[i][k] = leftSide[addedItemIndex[j]][0];
                }
            }
        }
    }
}

void eliminateInaccessibleSymbolsLength() {
    vector<string> symbols = {"Z", "V", "W"};
    vector<string> eliminateLength;
    vector<string> eliminateLengthSwap;
    // remove symbols with production of length 2+
    for (int i = 0; i < rightSide.size(); i++) {
        if (rightSide[i].size() > 2) {
            string element = string(1, rightSide[i][1]) + rightSide[i][2];
            eliminateLength.push_back(element);
            eliminateLengthSwap.push_back(symbols[eliminateLength.size() - 1]);
        }
    }
    // filter unique symbols
    eliminateLength.erase(unique(eliminateLength.begin(), eliminateLength.end()), eliminateLength.end());

    // swap unique in first array
    for (int k = 0; k < eliminateLength.size(); k++) {
        for (int i = 0; i < rightSide.size(); i++) {
            if (rightSide[i].size() > 2) {
                for (int j = 0; j < rightSide[i].size(); j++) {
                    if (rightSide[i][j] && rightSide[i][j + 1]) {
                        if (rightSide[i][j] == eliminateLength[k][0] && rightSide[i][j + 1] == eliminateLength[k][1]) {
                            rightSide[i][j] = eliminateLengthSwap[k][0];
                            rightSide[i][j + 1] = rightSide[i][j + 2];
                            rightSide[i][j + 2] = ' ';
                        }
                    }
                }
            }
        }
    }
}

void printCNF() {
    cout << "\n CNF = {\n";
    for (int i = 0; i < leftSide.size(); i++) {
        cout << i << ". " << leftSide[i] << " -> " << rightSide[i] << ";\n";
    }
    cout << "}";
}

int main() {
    // Varianta 20
    printArray(rightSide, productionLength);
    // Search productions with epsilon
    int epsilonProdIndex = searchEpsilon(rightSide);
    // set nonterminal epsilon
    string nonterminalEpsilon = leftSide[epsilonProdIndex];
    // Eliminate ε productions
    leftSide[epsilonProdIndex] = "";
    rightSide[epsilonProdIndex] = "";
    leftSide = shiftRight(leftSide, epsilonProdIndex, false);
    rightSide = shiftRight(rightSide, epsilonProdIndex, true);
    cout << "Remove epsilon: \n";
    printArray(rightSide, productionLength);

    // search productions with nonterminal symbols found with epsilon
    eliminationOfRenaming(leftSide, nonterminalEpsilon);
    cout << "Elimination of renaming: \n";
    printArray(rightSide, productionLength);
    printArray(leftSide, productionLength);

    // eliminate inaccessible symbols
    eliminateInaccessibleSymbols(leftSide);
    cout << "Eliminate inaccessible symbols: \n";
    printArray(rightSide, rightSide.size());
    printArray(leftSide, leftSide.size());

    eliminateNonProductiveSymbols(leftSide);
    cout << "Eliminate non productive symbols: \n";
    printArray(rightSide, rightSide.size());
    printArray(leftSide, leftSide.size());

    cout << "Eliminate non productive symbols length: \n";
    eliminateInaccessibleSymbolsLength();
    printArray(rightSide, rightSide.size());
    printArray(leftSide, leftSide.size());


    printCNF();
    return 0;
}
